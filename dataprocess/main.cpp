#include <iostream>
#include <io.h>
#include <vector>
#include <string>
#include<fstream>
#include <iomanip>

using namespace std;

bool strcat(char* a, char* b);
void findfile(string path, vector<string>& file);
void readdata(string path, vector<string> file, string outfile);

int main(int argc, char* argv[])
{
	char* optf = "-f";
	char* opto = "-o";
	bool existpath = false;
	string filepath;
	string outfile="";
	vector<string> files;

	for (int i = 0; i < argc; i++)
	{
		if (strcat(argv[i], optf))
		{
			existpath = true;
			filepath = argv[i + 1];
			findfile(filepath, files);
		}
		if (strcat(argv[i], opto))
			outfile = argv[i + 1];
	}
	
	readdata(filepath, files, outfile);
	if (!existpath)
		cout << "please input the filepath" << endl;
	return 0;
}

bool strcat(char* a, char* b)
{
	int l1 = strlen(a);
	int l2 = strlen(b);
	if (l1 != l2)
		return false;
	for (int i = 0; i < l1;i++)
	if (a[i] != b[i])
		return false;
	return true;
}
void findfile(string path, vector<string>& file)
{
	path = path + "\\*.log";
	_finddata_t fileInfo;
	long handle = _findfirst(path.c_str(), &fileInfo);
	file.push_back(fileInfo.name);
	while (_findnext(handle, &fileInfo) == 0)
		file.push_back(fileInfo.name);
}
void readdata(string path,vector<string> file,string outfile)
{
	ofstream fout;
	if (outfile=="")
		outfile = path + "\\" + "result.txt";
	fout.open(outfile);
	fout << std::left << setw(30) << "filename" << " " << "Total Frames |   Bitrate     Y-PSNR    U-PSNR    V-PSNR    YUV-PSNR" << "  " << "Total Time" << endl;
	for (int i = 0; i < file.size(); i++)
	{
		string filepath = path + "\\" + file[i];
		ifstream fin(filepath);
		string line;
		while (getline(fin, line) )
		{
			if (line.find("SUMMARY") != string::npos)
			{
				getline(fin, line);
				getline(fin, line);
				fout <<std::left<< setw(30) << file[i] << ":" << line<<"  ";
			}
			if (line.find("Total Time") != string::npos)
			{
				for (int j = 0; j < line.length();j++)
				if ((line[j] >= '0'&&line[j] <= '9') || line[j] == '.')
					fout << line[j];
			}			
		}
		fout << endl;
		fin.close();
	}	
	fout.close();
}

